import certifi
import docx
import urllib3

"""
    Esta funcion lee archivos .txt alojados en una direccion web y devuelve el texto que contiene. 
    Esta bien recibe como parametro la direccion donde se encuentra alojado el .txt a leer.
"""


def read_txt(target_url):
    # Genera una conexion http segura con el servidor.
    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=certifi.where())
    # Una vez lista la conexion solo hacemos el request a la direccion ingresada
    response = http.request('GET', target_url)
    # Se usa el formato ISO-8859-1 porque es mas compatible con todos los caracteres.
    data = response.data.decode('ISO-8859-1')
    return data


"""
    Esta funcion lee archivos .docx alojados localmente y devuelve el texto que contiene. 
    Esta bien recibe como parametro el nombre del archivo .docx  a leer.
    Usa la biblioteca docx que maneja la extraccion de texto de una forma mas apropiada, dando la funcionalidad de extraer el texto en formato plano.
"""


def read_docx(filename):
    # Docx nos un objeto docx para poder extraer el texto con sus funciones propias
    doc = docx.Document(filename)
    fullText = []
    # Extrae cada linea de texto del documento para generar una solo variable texto plano
    for para in doc.paragraphs:
        fullText.append(para.text)
    # Devuelve el texto plano extraido
    return '\n'.join(fullText)
