"""
    index.py; centro de control del framework de flask.
    Copyright (C) 2019  Luis Arias,Fernando Rojas, Kenneth Vargas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses>
"""

import os

from flask import Flask, render_template, url_for, flash, redirect, json, request
from form import RegistrationForm, LoginForm, ExportForm, AnalizarForm
from read_files import read_txt, read_docx
from firebase_connection import connection
from controller import highlight_text

# Definición de que va a ser una aplicación basada en flask
app = Flask(__name__)
# Encriptación de la llave secreta para el login y el registro
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
# Almacenado de la conexión con Firebase
storage = connection()

"""
    Ruta para la página de home. Utilizar los métodos de Get y Post para comunicarse con el HTML.
    En esta sección se realiza el almacenemiento del archivo o texto con Firebase.
    Por otro parte utiliza el form AnalizarForm(), para el diseño del HTML
    Retorna un render_template a la página html de home
"""


@app.route('/', methods=['GET', 'POST'])
def home():
    # Form para analizar-> Para mayor información ver form.py
    form = AnalizarForm()

    if request.method == 'POST':
        upload = request.files['upload']  # Toma el documento cargado
        # Crea la direccion donde se va a alojar el documento en Firebase Storage
        location = "document/" + upload.filename
        if location == "document/":  # Se ejecuta si no se carga un documento
            return render_template('home.html', title='Home', form=form)
        else:
            # Sube el documento a Firebase Storage
            storage.child(location).put(upload)
            # Devuelve la extension que tiene el documento
            extension = upload.filename.split('.')[1]
            filename_document = upload.filename
            if extension == "txt":
                # Devuelve el link donde se encuentra alojado el documento
                link = storage.child(location).get_url(None)
                # Extrae el texto del documento .txt que se encuentra en el Firebase Storage.
                data_from_file = read_txt(link)
                # Se ejecuta la funcion que analiza el texto y devuelve las etiquetas con el codigo html
                analyzed_text = highlight_text(data_from_file)
                return render_template('home.html', title='Home', data_file=data_from_file, form=form,
                                       a_text=analyzed_text)
            elif extension == "docx":
                # Baja una copia temporal del documento .docx de donde se encuentra alojado
                storage.child(location).download(filename_document)
                # Extrae el texto del documento .docx que se encuentra en el Firebase Storage.
                data_from_file = read_docx(filename_document)
                # Borra la copia temporal del documento .docx
                os.remove(filename_document)
                # Se ejecuta la funcion que analiza el texto y devuelve las etiquetas con el codigo html
                analyzed_text = highlight_text(data_from_file)
                return render_template('home.html', title='Home', data_file=data_from_file, form=form,
                                       a_text=analyzed_text)
    return render_template('home.html', title='Home', form=form)


"""
    Ruta para la página de about.
    Retorna un render_template a la página html de about
"""


@app.route('/about')
def about():
    return render_template('about.html', title='About')


"""
    Ruta para la página de settings. Utilizar los métodos de Get y Post para comunicarse con el HTML.
    Acá se genera el .json donde se almacenan todos los datos de la configuración
    Por otro parte utiliza el form ExportFrom(), para el diseño del HTML
    Retorna un render_template a la página html de settings
"""


@app.route('/settings', methods=['GET', 'POST'])
def settings():
    # Form para Export-> Para mayor información ver form.py
    form = ExportForm()
    # Si el boton de submit ha sido presionado haga esto, funciona para exportar y guardar.
    if form.is_submitted():
        # Asignación de los valores del html
        col_A = request.form["color_A"]
        col_B = request.form["color_B"]
        dif = request.form["dificultad"]
        typ = form.type.data
        # Creación del json
        try:
            config_project = {'Color_Min': str(col_A),
                              'Color_Max': str(col_B),
                              'Dificultad': str(dif),
                              'Tipo': str(typ)}

            with open('data.json', 'w') as file:  # Abrir el archivo y guardarlo como un .json
                json.dump(config_project, file, indent=4)
            # Guardando el data.json donde estan los ajustes del proyecto.
            flash(f'Exportado correctamente', 'success')
        except Exception as e:
            flash(f'Error exportando', 'danger')
            print(str(e))  # En caso de error lo imprimi en este medio.
        return redirect(url_for('home'))

    return render_template('settings.html', title='Settings', form=form)


"""
    Ruta para la página de register. Utilizar los métodos de Get y Post para comunicarse con el HTML.
    Por otro parte utiliza el form RegisterForm(), para el diseño del HTML
    Retorna un render_template a la página html de register
"""


@app.route('/register', methods=['GET', 'POST'])
def register():
    # Form para register-> Para mayor información ver form.py
    form = RegistrationForm()
    if form.is_submitted():
        flash(f'Cuenta creada para {form.username.data}!', 'success')  # Mostrar msj de cuenta creada
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


"""
    Ruta para la página de login. Utilizar los métodos de Get y Post para comunicarse con el HTML.
    En esta sección se realiza la validación de los datos ingresados.
    Por otro parte utiliza el form AnalizarForm(), para el diseño del HTML
    Retorna un render_template a la página html de login
"""


@app.route('/login', methods=['GET', 'POST'])
def login():
    # Form para login-> Para mayor información ver form.py
    form = LoginForm()
    if form.validate_on_submit():  # Espera para que el submit se presione
        # Validación de los datos ingresados
        if form.email.data == 'admin@asivipcom.com' and form.password.data == 'admin':
            flash('Sesión iniciada!', 'success')
            return redirect(url_for('home'))
        else:
            flash(
                'Error en iniciar sesión. Por favor revise el correo y la contraseña', 'danger')
    return render_template('login.html', title='Login', form=form)


"""
    Ruta para el método de search. Utilizar los métodos de Get y Post para comunicarse con el HTML.
    En esta sección se va a comunicar con la base de datos, para mostrar los regionalismos.
    Utiliza el form de analizar, pero es porque el render_template requiere de este formulario para 
    poder funcionar.
    Retorna un render_template a la página html de home
"""


@app.route('/search', methods=['GET', 'POST'])
def search():
    datos = "No hay datos"
    form1 = AnalizarForm()
    if request.method == 'POST':
        datos = " Perro, Chucho, Honduras\n Perro, Zaguate, Costa Rica\n Perro, Can, Chile"
    return render_template('home.html', title='Home', form=form1, data_search=datos)


"""
    Ejecución del name, en este caso es un flask.
    Debug=1; esta activado, esto quiere decir que cuando se realice un cambio con recargar la 
    página se verá reflejado el cambio realizado.
"""
if __name__ == '__main__':
    app.run(DEBUG=1)
