# Proyecto Asivipcom 

## Asivipcom: Asistente Visualizador de Palabras Complejas

Este proyecto es un asistente para el análisis de la complejidad de textos, esto con el fin de poder simplificar su redacción. De esta manera podemos generar documentos que sean más fáciles de entender para todas las comunidades de la región. El mismo ha sido desarrollado en conjunto con el programa de escuela para todos, ICECU y el Tecnológico de Costa Rica, este último siendo que desarrolla la aplicación web.