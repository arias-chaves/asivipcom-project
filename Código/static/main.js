(function() {

  // Esta variable inicializa la estructura necesaria para guardar los valores del resumen del texto
  let data = {
    paragraphs: 0,
    sentences: 0,
    words: 0,
    letters: 0
  };

  // Esta funcion es la encargada analizar el texto y generar los valores que tendra el resumen del texto
  function format() {
    data = {
      paragraphs: 0,
      sentences: 0,
      words: 0,
      letters: 0
    };
    ("use strict");
    // Extrae el texto que contiene el editor de texto
    let inputArea = document.getElementById("inputTextToSave");
    let text = inputArea.value;
    // Parte el texto donde encuentre un salto de linea
    let paragraphs = text.split("\n");
    for (let i = 0; i < paragraphs.length; i++){
      if (paragraphs[i] != ""){
        data.paragraphs += 1;
      }
    }
    // Ciclo que recorre todas las oraciones que tenga el texto para analizarlas
    paragraphs.map(p => getNumberOfSentencesAndWords(p));
    // Ejecuta la funcion de resumen del texto
    printSummary();
  }
  // Esta hace que la funcion format tenga un alcance global
  window.format = format;

  // Esta funcion ubica las etiquetas html donde se debe imprimir los valores del resumen del texto
  function printSummary() {
      // Imprime la cantidad de parrafos
      document.querySelector("#paragraphs").innerHTML = `Parrafos: ${data.paragraphs}`;
      // Imprime la cantidad de oraciones
      document.querySelector("#sentences").innerHTML = `Oraciones: ${data.sentences}`;
      // Imprime la cantidad de palabras
      document.querySelector("#words").innerHTML = `Palabras: ${data.words}`;
      // Imprime la cantidad de letras
      document.querySelector("#letters").innerHTML = `Letras: ${data.letters}`;
  }

  // Esta se encarga de generar el numero de palabras, letras y oraciones que contiene el texto.
  // Recibe como parametro una sola oracion para analizarla.
  // Guarda los valores respectivos en la estructura que lleva el conteo del resumen del texto.
  function getNumberOfSentencesAndWords(p) {
    // Recorre el texto para encontrar la cantidad de oraciones, la ubica donde haya un punto.
    let sentences = getSentenceFromParagraph(p + " ");
    if (sentences[0] != " ."){
      // Suma la cantidad de oraciones encontradas.
      data.sentences += sentences.length;
    }
    sentences.map(sent => {
      // A partir de un regex generado, elimina los caracteres que no sean ni letras ni numeros de la oracion ingresada
      let cleanSentence = sent.replace(/[^a-z0-9. ]/gi, "") + ".";
      // Parte la oracion en palabras, y hace el conteo de cuantas son.
      let words = cleanSentence.split(" ").length;
      // Concatena todas las palabras sin espacios para saber cuantas letras contiene la oracion.
      let letters = cleanSentence.split(" ").join("").length;
      if (cleanSentence != " .."){
        data.words += words; // Se suma la cantidad de palabras registradas
        data.letters += letters-2; // Se resta un -2 porque se eliminan los dos puntos, ingresados para saber donde termina la oracion.
      }

      return sent;
    });
  }

  // Esta funcion se encarga de hacer en oraciones los parrafos del texto ingresado. Y devuelve las oraciones separadas para analizarlas.
  function getSentenceFromParagraph(p) {
    let sentences = p
      .split(". ")
      .filter(s => s.length > 0)
      .map(s => s + ".");
    return sentences;
  }
})();
