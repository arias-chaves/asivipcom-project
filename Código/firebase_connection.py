import pyrebase

"""
    Esta es la conexion que se usa para utilizar el servicio de Firebase Storage.
    Esta conecta especificamente con una cuenta generada propia para este proyecto, a partir de las credenciales que tiene asignada esta cuenta.
    Esta funcion devuelve la conexion necesaria utilizar las funciones propias del servicio.
"""


def connection():
    config = {
        "apiKey": "AIzaSyBe_bpIYXEvidIedBIPdMnWVFu9rmoctVI",
        "authDomain": "asivipcom.firebaseapp.com",
        "databaseURL": "https://asivipcom.firebaseio.com",
        "projectId": "asivipcom",
        "storageBucket": "asivipcom.appspot.com",
        "messagingSenderId": "397156663519"
    }

    # Inicializa la conexion a partir de las credenciales proporcionadas
    firebase = pyrebase.initialize_app(config)
    # Genera un conexion con un servicio Firebase Storage.
    storage = firebase.storage()
    return storage
