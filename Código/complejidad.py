# Biblioteca de complejidad documental
import random


def analizar(texto, granularidad = "oracion"):

    json = "{"

    lista_oraciones = texto.split(".")

    if len(lista_oraciones) == 1:
        return "{ ['"+lista_oraciones[0]+"', "+str(random.random()) + ", ['telemaco', 'cofradía', 'argot']] }"

    elif len(lista_oraciones) > 1:
        json += "['"+lista_oraciones[0]+"', "+str(random.random()) + ", ['telemaco', 'cofradía', 'argot']]"

    for oracion in lista_oraciones[1:]:
        json += ", "
        json += "['"+oracion+"', "+str(random.random()) + ", ['telemaco', 'cofradía', 'argot']]"

    json += "}"

    return json


