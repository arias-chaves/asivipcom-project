from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField, SubmitField, BooleanField, SelectField
from wtforms.validators import DataRequired, EqualTo, length


class RegistrationForm(FlaskForm):
    username = StringField('Usuario', [DataRequired(), length(min=4, max=15)])

    email = StringField('Email', [DataRequired(), length(min=6, max=35)])

    password = PasswordField('Contraseña',
                             [DataRequired(), EqualTo('confirm', message='Passwords must match')])

    confirm = PasswordField('Repetir Contraseña')

    submit = SubmitField('Registrarse')


class LoginForm(FlaskForm):
    email = StringField('Email', [DataRequired(), length(min=6, max=35)])

    password = PasswordField('Contraseña', [DataRequired()])

    remember = BooleanField('Guardar sesión')

    submit = SubmitField('Iniciar Sesión')


class ExportForm(FlaskForm):
    type = SelectField('Opciones', choices=[('PA', 'Palabra'), ('LI', 'Línea'), ('TE', 'Texto')])

    submitE = SubmitField('Exportar')

    submitG = SubmitField('Guardar')


class AnalizarForm(FlaskForm):
    submitAnalize = SubmitField('Analizar')
