import complejidad
import ast


"""
    Esta funcion es la encargada de analizar el texto de los documentos cargados, con la ayuda de la biblioteca proporcionada
    generara una estructura json con el texto, la dificultad y las palabras desconocidas.
    Recibe como parametro el texto a analizar.
    Devuelve el codigo html con el tipo de subrayado que pertenece a cada oracion dependiendo del rango de su dificultad.
"""


def highlight_text(text):
    """
    En esta espacio de codigo lo que se hace es convertir el string que devuelve la biblioteca a un objeto lista.
    """
    # Utiliza la funcion analizar() provista por la biblioteca para analizar texto
    # La variable data va a contener un string con la estructura de un JSON devuelta por la biblioteca
    analyzed_text = complejidad.analizar(text)

    # Se elimina el corchete de inicio y final
    analyzed_text = analyzed_text[1:-1]

    # Se convierte el string a un objeto tupla
    # Se agregan estos caracteres \r \n ya que la funcion "ast.literal_eval" no los reconoce como literales, sino como caracteres de texto
    analyzed_text = ast.literal_eval(analyzed_text.replace('\r', '\\r').replace('\n', '\\n'))

    # En este ciclo se va a eliminar los caracteres \r \n agregados en el paso anterior.
    for x in range(len(analyzed_text)):
        analyzed_text[x][0] = analyzed_text[x][0].replace('\r\n', '')

    # Se inicializa la variable que contendra el codigo html.
    code = ""

    # Este ciclo recorrera la salida que genera la biblioeca, analizando en que rango de dificultad pertece la oracion.
    # Las clases que tienen los tags "class" son los colores definidos en la parte de configuracion de la pagina.
    # Si la dificultad de la oracion es menor al rango establecido, entonces se agrega su codigo para subrayar el texto.
    for x in range(len(analyzed_text)):
        if analyzed_text[x][1] <= 0.16:
            tags = "<span class="+'"rangeOneSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

        elif analyzed_text[x][1] <= 0.32:
            tags = "<span class="+'"rangeTwoSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

        elif analyzed_text[x][1] <= 0.48:
            tags = "<span class="+'"rangeThreeSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

        elif analyzed_text[x][1] <= 0.64:
            tags = "<span class="+'"rangeFourSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

        elif analyzed_text[x][1] <= 0.8:
            tags = "<span class="+'"rangeFiveSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

        elif analyzed_text[x][1] <= 1:
            tags = "<span class="+'"rangeSixSentence"'+">"+analyzed_text[x][0]+"</span>"
            code += " " + tags

    return code